'use strict';

angular.module('jsSparkUiApp')
    .controller('NavbarCtrl', function ($scope, $location, Auth, navbarCatService, _, $state) {
        $scope.menu = [
            {
                'title': 'Free cats',
                'link': '/',
                permission: () => {
                    return !$scope.isLoggedIn();
                }
            },
            {
                'title': 'Premium cats',
                'link': '/premium',
                permission: () => {
                    return !$scope.isLoggedIn();
                }
            },
            {
                'title': 'Dashboard',
                'link': '/websites',
                permission: () => {
                    return $scope.isContentProvider();
                }
            },
            {
                'title': 'How to',
                'link': '/websiteshowto',
                permission: () => {
                    return $scope.isContentProvider();
                }
            },
            {
                'title': 'For Business Clients',
                'link': '/for-clients',
                permission: () => {
                    return $scope.isClient();
                }
            }
        ];

        $scope.isCollapsed = true;
        $scope.isLoggedIn = Auth.isLoggedIn;
        $scope.isAdmin = Auth.isAdmin;
        $scope.isClient = Auth.isClient;
        $scope.isContentProvider = Auth.isContentProvider;
        $scope.getCurrentUser = Auth.getCurrentUser;

        $scope.logout = function () {
            Auth.logout();
            $location.path('/login');
        };

        //TODO trailing /:id
        $scope.isActive = function (route) {
            if ($state.current.name === 'main') {
                if ($scope.isContentProvider) {
                    $state.go('websites');
                }
                if ($scope.isClient) {
                    $state.go('for-clients');
                }
            }

            // console.log($location.path(), route);
            return _.isEqual($location.path(), route);
        };

        $scope.selected = "";
        $scope.goVideo = function () {
            if ($scope.selected === $scope.videos[1]) {
                $state.go("premium", {videoId: 1});
            } else if ($scope.selected === $scope.videos[2]) {
                $state.go("premium", {videoId: 2});
            } else {
                $location.path('/home');
            }
        };

        $scope.videos = navbarCatService;

    });
