'use strict';

angular.module('jsSparkUiApp')
    .controller('ForClientsCtrl', function ($scope, $http, socket, Auth, $timeout, moment) {
        $scope.clients = [];

        $http.get('/api/clients').success(function (clients) {
            $scope.clients = clients;
            socket.syncUpdates('client', $scope.clients);
        });

        //TODO move to alert service
        $scope.closeAlert = function (index) {
            console.warn('closing', index);
        };

        $scope.$on('$destroy', function () {
            socket.unsyncUpdates('client');
        });

        $scope.getCurrentUser = Auth.getCurrentUser;

        $scope.required = {cpu: 2};

        //TODO
        $scope.scaleCpu = (newVal) => {
            $http.post('/api/scale/' + $scope.required.cpu).success(function (response) {
                console.log('scaled', response);
                //$scope.clients = clients;
                //socket.syncUpdates('client', $scope.clients);
            });
        };

        // start autodesk computing ques
        //$http.post('/api/autodesk', {}).success(console.log).catch(console.error);

        //TODO
        $scope.jobs = [
            {name: 'Neural network train 1.6', progress: 55, eta: moment().add(24, 'minutes').format(), type: 'normal'},
            {
                name: 'Malaria prevalence prediction',
                progress: 25,
                eta: moment().add(2, 'days').format(),
                type: 'warning'
            }
        ];

        // TODO kichoo mozna z moment.js wziac ladniejsze datatime?
        $scope.labels = ["January", "February", "March", "April", "May", "June", "July"];
        $scope.series = [$scope.jobs[0].name, $scope.jobs[1].name];

        $scope.data = [
            [65, 59, 80, 81, 56, 55, 40],
            [28, 48, 40, 19, 86, 27, 90]
        ];
        $scope.onClick = function (points, evt) {
            console.log(points, evt);
        };

        // TODO Simulate async data update
        $timeout(function () {
            $scope.data = [
                [28, 48, 40, 19, 86, 27, 90],
                [65, 59, 80, 81, 56, 55, 40]
            ];
        }, 3000);

        $http.get('/api/clients').success(function (clients) {
            $scope.clients = clients;
            console.log(clients);
            socket.syncUpdates('client', $scope.clients);
        });

        //TODO
        $timeout(function () {
            $scope.jobs = $scope.jobs.map(j => {
                j.progress += ( Math.random() * 20);
                return j
            });
        }, 5000);

    });

