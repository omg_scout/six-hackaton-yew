//@six-hacaton 2016
angular.module('jsSparkUiApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('for-clients', {
                url: '/for-clients',
                templateUrl: 'app/for-clients/for-clients.html',
                controller: 'ForClientsCtrl'
            });
    });
