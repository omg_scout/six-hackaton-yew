'use strict';

angular.module('jsSparkUiApp')
    .controller('MainCtrl', function ($scope, $sce, $http, socket) {

        $scope.config = {
            preload: "none",
            sources: [
                {
                    //src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.mp4"),
                    src: $sce.trustAsResourceUrl("assets/video/The-biggest-cat-fails-2013.mp4"),
                    type: "video/mp4"
                }
            ],
            tracks: [
                {
                    src: "http://www.videogular.com/assets/subs/pale-blue-dot.vtt",
                    kind: "subtitles",
                    srclang: "en",
                    label: "English",
                    default: ""
                }
            ],
            theme: {
                url: "http://www.videogular.com/styles/themes/default/latest/videogular.css"
            }
        };
    });
