angular.module('jsSparkUiApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('premium', {
                url: '/premium?videoId',
                templateUrl: 'app/premium/premium.html',
                controller: 'PremiumCtrl'
            })
    });
