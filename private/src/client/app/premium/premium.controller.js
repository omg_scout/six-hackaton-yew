'use strict';

angular.module('jsSparkUiApp')
    .controller('PremiumCtrl', function ($scope, $sce, $stateParams, $http, socket) {
        console.log($stateParams, $stateParams.videoId);

        var sources = [];

        if ($stateParams.videoId && $stateParams.videoId === 1) {
            sources = [{
                src: $sce.trustAsResourceUrl("assets/video/Funny-cat-compilation.mp4"),
                type: "video/mp4"
            }]
        } else {
            sources = [{
                src: $sce.trustAsResourceUrl("assets/video/Cats-scared-of-Cucumbers-Compilation.mp4"),
                type: "video/mp4"
            }]
        }

        $scope.config = {
            preload: "none",
            sources: sources
        };
    });

