angular.module('jsSparkUiApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('websites', {
                url: '/websites',
                templateUrl: 'app/websites/websites.html',
                controller: 'WebsitesCtrl'
            });
    });
