'use strict';

angular.module('jsSparkUiApp')
    .controller('WebsitesCtrl', function ($scope, $http, socket, $timeout) {
        $scope.clients = [];

        $http.get('/api/clients').success(function (clients) {
            $scope.clients = clients;
            socket.syncUpdates('client', $scope.clients);
        });

        //TODO move to alert service
        $scope.closeAlert = function (index) {
            console.warn('closing', index);
        };

        $scope.$on('$destroy', function () {
            socket.unsyncUpdates('client');
        });

        $scope.left = {
            labels: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
            data: [[0, 10, 14, 8, 15, 18, 21]]
        }

        $scope.right = {
            labels: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
            data: [[0, 3, 4, 7, 8, 12, 16]]
        }        

        $scope.balance = 21.43;

          // // Simulate async data update
          // $timeout(function () {
          //   $scope.left.labels.push("sun");
          //   $scope.left.data.push(16);

          //   $scope.right.labels.push("sun");
          //   $scope.right.data.push(19);
          // }, 3000);
    });

