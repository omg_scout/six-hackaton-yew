angular.module('jsSparkUiApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('websiteshowto', {
                url: '/websiteshowto',
                templateUrl: 'app/websites_howto/websites_howto.html',
                controller: 'WebsitesHowtoCtrl'
            });
    });
