'use strict';

angular.module('jsSparkUiApp')
    .controller('WebsitesHowtoCtrl', function ($scope, $http, socket) {
        $scope.clients = [];

        $http.get('/api/clients').success(function (clients) {
            $scope.clients = clients;
            socket.syncUpdates('client', $scope.clients);
        });

        //TODO move to alert service
        $scope.closeAlert = function (index) {
            console.warn('closing', index);
        };

        $scope.$on('$destroy', function () {
            socket.unsyncUpdates('client');
        });

        $scope.codeSnippet = `<script>
    (function (m, a, r, g, o, s, i) {
        i['sparking'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o), m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.data-spark.com/snippet.js', 'spark');
    spark('start', '$your-token');
    spark('send', 'start');
</script>`
    });

